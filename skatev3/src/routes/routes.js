import { Checkout } from "../containers/checkout/checkout";
import { Invoice } from "../containers/invoice/invoice";

const routes = [
  {
    path: "/invoice",
    exact: true,
    component: Invoice,
  },
  {
    path: "/checkout",
    exact: true,
    component: Checkout,
  }
];
export default routes;
