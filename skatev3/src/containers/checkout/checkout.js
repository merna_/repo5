import React from "react";
import { Invoice } from "./invoice/invoice";
import './checkout.css';

export const Checkout = () => {
  return (
    <div className="checkout">
      <div className="container">
      <div className="row">
      <div className="col-lg-5 col-md-6">
        <Invoice />
      </div>
      <div className="col-md-6"></div>
    </div>
    </div>
    </div>
  );
};
