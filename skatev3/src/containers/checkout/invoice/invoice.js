import React from "react";
import { InvoiceFromTo } from "./invoiceFromTo/invoiceFromTo";
import { InvoiceHeader } from "./invoiceHeader/invoiceHeader";
import { InvoiceTotal } from "./invoiceTotal/invoiceTotal";
import { ViewInvoice } from "./viewInvoice/viewInvoice";
import './invoice.css'
export const Invoice = () => {
  return (
    <div className="checkoutInvoice">
      <InvoiceHeader />
      <InvoiceFromTo />
      <InvoiceTotal />
      <ViewInvoice />
    </div>
  );
};
