import React from 'react';
import {Logo} from '../../../../elements/logo/logo';
import './invoiceHeader.css';
export const InvoiceHeader = () => {
    return (
        <div className="invoiceHeader container">
        <div className="row">
      <div>
        <p>Invoice# 289734567</p>
        <span>Pending</span>
      </div>
      <Logo className="logo"></Logo>
    </div>
    </div>
    );
};

