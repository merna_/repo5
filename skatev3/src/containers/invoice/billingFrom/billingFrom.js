import React from "react";
import "./billingFrom.css";
export const BillingFrom = () => {
  return (
    <div className="billingFrom">
      <h5>Billing From</h5>
      <p>
        Ryets Inc. <br />
        John Doe
        <br /> 332 South Wayside Rd, <br />
        Houston, TX <br />
        United States
      </p>
    </div>
  );
};
