import React, { Component } from "react";
import { Links } from "../../../elements/links/links";
import "./invoiceTable.css";
import { Icon } from "../../../elements/icon/icon";
import { Button } from "../../../elements/button/button";

export class InvoiceTable extends Component {
  state = {
    thead: [
      { class: "col-md-5 d-sm-none d-none d-md-block", name: "Car" },
      { class: "col-md-4 d-sm-none d-none d-md-block", name: "Service" },
      { class: "col-md-3 d-sm-none d-none d-md-block", name: "Price" },
    ],
    prices: [
      { name: "subtotal", price: 1700.0 },
      { name: "fees", price: 35.36 },
      { name: "total", price: 1735.36 },
    ],
  };
  render() {
    const { thead } = this.state;
    return (
      <div className="invoiceTable">
        <div className="row headBorder">
          {thead.map((items, id) => {
            return (
              <div key={id} className={items.class}>
                {items.name}
              </div>
            );
          })}
        </div>
        <div className="row border">
          <div className="col-md-5 car">
            <div className="row">
              <h5>2020 MercedesBenz C300</h5>
              <Links to="/report">
                <Icon
                  xmlns="http://www.w3.org/2000/svg"
                  xmlnsXlink="http://www.w3.org/1999/xlink"
                  width="14"
                  height="14"
                  viewBox="0 0 14 14"
                  fill="#0078d4"
                >
                  <defs>
                    <clipPath id="a">
                      <rect
                        className="a"
                        width="14"
                        height="14"
                        transform="translate(206.525 319)"
                      />
                    </clipPath>
                  </defs>
                  <g className="b" transform="translate(-206.525 -319)">
                    <g transform="translate(206.525 319)">
                      <path
                        className="c"
                        d="M13.877,4.071,10.086.134a.438.438,0,0,0-.752.3v1.9H9.188A5.694,5.694,0,0,0,3.5,8.021V8.9a.432.432,0,0,0,.341.418.391.391,0,0,0,.1.012.453.453,0,0,0,.4-.249,4.785,4.785,0,0,1,4.3-2.66h.693v1.9a.438.438,0,0,0,.752.3l3.792-3.937A.438.438,0,0,0,13.877,4.071Zm0,0"
                      />
                      <path
                        className="c"
                        d="M12.25,14H1.75A1.752,1.752,0,0,1,0,12.25V4.083a1.752,1.752,0,0,1,1.75-1.75H3.5A.583.583,0,0,1,3.5,3.5H1.75a.584.584,0,0,0-.583.583V12.25a.584.584,0,0,0,.583.583h10.5a.584.584,0,0,0,.583-.583V7.583a.583.583,0,1,1,1.167,0V12.25A1.752,1.752,0,0,1,12.25,14Zm0,0"
                      />
                    </g>
                  </g>
                </Icon>
              </Links>
            </div>
            <p className="fromTo">
              AutoDeal, Inc
              <Icon
                xmlns="http://www.w3.org/2000/svg"
                className="arrow"
                xmlnsXlink="http://www.w3.org/1999/xlink"
                width="11"
                height="11"
                viewBox="0 0 11 11"
                fill="#888"
              >
                <defs>
                  <clipPath id="a">
                    <rect
                      className="a"
                      width="11"
                      height="11"
                      transform="translate(304 758)"
                    />
                  </clipPath>
                </defs>
                <g className="b" transform="translate(-304 -758)">
                  <g transform="translate(438.937 842.549)">
                    <path
                      className="c"
                      d="M-123.937-79.043a.434.434,0,0,0-.159-.343l-3.94-3.94a.486.486,0,0,0-.355-.171.445.445,0,0,0-.465.44.452.452,0,0,0,.135.33l1.554,1.578,1.872,1.713-1.407-.073h-7.782a.434.434,0,0,0-.453.465.44.44,0,0,0,.453.465h7.782l1.407-.073-1.872,1.713-1.554,1.566a.473.473,0,0,0-.135.33.445.445,0,0,0,.465.44.456.456,0,0,0,.33-.147l3.964-3.964A.4.4,0,0,0-123.937-79.043Z"
                    />
                  </g>
                </g>
              </Icon>
              West Port
            </p>
          </div>
          <div className="col-md-4 col-7 ">
            <div className="service">
              Transportation
              <span className="has-tooltip">
                <Icon
                  className="icon "
                  xmlns="http://www.w3.org/2000/svg"
                  width={15}
                  height={15}
                  viewBox="0 0 18 18"
                >
                  <defs>
                    <clipPath id="prefix__a">
                      <path
                        transform="translate(1039 531)"
                        stroke="#707070"
                        fill="#707070"
                        d="M0 0h18v18H0z"
                      />
                    </clipPath>
                    <style>{".prefix__c{fill:#707070}"}</style>
                  </defs>
                  <g
                    transform="translate(-1039 -531)"
                    clipPath="url(#prefix__a)"
                  >
                    <path
                      className="prefix__c"
                      d="M1054.572 533.061h-13.162a2.425 2.425 0 00-2.41 2.425v9.027a2.429 2.429 0 002.428 2.428h13.144a2.429 2.429 0 002.428-2.428v-9.027a2.429 2.429 0 00-2.428-2.425zm.755 10.933a1.309 1.309 0 01-1.327 1.292h-12a1.309 1.309 0 01-1.331-1.295v-7.983a1.309 1.309 0 011.331-1.295h11.979a1.309 1.309 0 011.331 1.295v7.984z"
                    />
                    <path
                      className="prefix__c"
                      d="M1042.794 537.338a.483.483 0 00.486.486h2.9a.486.486 0 000-.971h-2.9a.483.483 0 00-.486.485zM1042.794 540a.483.483 0 00.486.486h9.44a.486.486 0 000-.971h-9.44a.483.483 0 00-.486.485zM1052.7 542.176h-9.42a.486.486 0 100 .971h9.44a.483.483 0 00.486-.486.511.511 0 00-.506-.485z"
                    />
                  </g>
                </Icon>
                <p className="container">
                  <span className="tooltip tooltip-left ">notes</span>
                </p>
              </span>
            </div>
            <div className="service">
              Transportation
              <span className="has-tooltip">
                <Icon
                  className="icon "
                  xmlns="http://www.w3.org/2000/svg"
                  width={15}
                  height={15}
                  viewBox="0 0 18 18"
                >
                  <defs>
                    <clipPath id="prefix__a">
                      <path
                        transform="translate(1039 531)"
                        stroke="#707070"
                        fill="#707070"
                        d="M0 0h18v18H0z"
                      />
                    </clipPath>
                    <style>{".prefix__c{fill:#707070}"}</style>
                  </defs>
                  <g
                    transform="translate(-1039 -531)"
                    clipPath="url(#prefix__a)"
                  >
                    <path
                      className="prefix__c"
                      d="M1054.572 533.061h-13.162a2.425 2.425 0 00-2.41 2.425v9.027a2.429 2.429 0 002.428 2.428h13.144a2.429 2.429 0 002.428-2.428v-9.027a2.429 2.429 0 00-2.428-2.425zm.755 10.933a1.309 1.309 0 01-1.327 1.292h-12a1.309 1.309 0 01-1.331-1.295v-7.983a1.309 1.309 0 011.331-1.295h11.979a1.309 1.309 0 011.331 1.295v7.984z"
                    />
                    <path
                      className="prefix__c"
                      d="M1042.794 537.338a.483.483 0 00.486.486h2.9a.486.486 0 000-.971h-2.9a.483.483 0 00-.486.485zM1042.794 540a.483.483 0 00.486.486h9.44a.486.486 0 000-.971h-9.44a.483.483 0 00-.486.485zM1052.7 542.176h-9.42a.486.486 0 100 .971h9.44a.483.483 0 00.486-.486.511.511 0 00-.506-.485z"
                    />
                  </g>
                </Icon>
                <p className="container">
                  <span className="tooltip tooltip-left ">notes</span>
                </p>
              </span>
            </div>
          </div>
          <div className="col-md-3 col-5 ">
            <div className="price">$150.00</div>
            <div className="price">$150.00</div>
          </div>
        </div>
        <div className="row border">
          <div className="col-md-5 car">
            <div className="row">
              <h5>2020 MercedesBenz C300</h5>
              <Links to="/report">
                <Icon
                  xmlns="http://www.w3.org/2000/svg"
                  xmlnsXlink="http://www.w3.org/1999/xlink"
                  width="14"
                  height="14"
                  viewBox="0 0 14 14"
                  fill="#0078d4"
                >
                  <defs>
                    <clipPath id="a">
                      <rect
                        className="a"
                        width="14"
                        height="14"
                        transform="translate(206.525 319)"
                      />
                    </clipPath>
                  </defs>
                  <g className="b" transform="translate(-206.525 -319)">
                    <g transform="translate(206.525 319)">
                      <path
                        className="c"
                        d="M13.877,4.071,10.086.134a.438.438,0,0,0-.752.3v1.9H9.188A5.694,5.694,0,0,0,3.5,8.021V8.9a.432.432,0,0,0,.341.418.391.391,0,0,0,.1.012.453.453,0,0,0,.4-.249,4.785,4.785,0,0,1,4.3-2.66h.693v1.9a.438.438,0,0,0,.752.3l3.792-3.937A.438.438,0,0,0,13.877,4.071Zm0,0"
                      />
                      <path
                        className="c"
                        d="M12.25,14H1.75A1.752,1.752,0,0,1,0,12.25V4.083a1.752,1.752,0,0,1,1.75-1.75H3.5A.583.583,0,0,1,3.5,3.5H1.75a.584.584,0,0,0-.583.583V12.25a.584.584,0,0,0,.583.583h10.5a.584.584,0,0,0,.583-.583V7.583a.583.583,0,1,1,1.167,0V12.25A1.752,1.752,0,0,1,12.25,14Zm0,0"
                      />
                    </g>
                  </g>
                </Icon>
              </Links>
            </div>
            <p className="fromTo">
              AutoDeal, Inc
              <Icon
                xmlns="http://www.w3.org/2000/svg"
                className="arrow"
                xmlnsXlink="http://www.w3.org/1999/xlink"
                width="11"
                height="11"
                viewBox="0 0 11 11"
                fill="#888"
              >
                <defs>
                  <clipPath id="a">
                    <rect
                      className="a"
                      width="11"
                      height="11"
                      transform="translate(304 758)"
                    />
                  </clipPath>
                </defs>
                <g className="b" transform="translate(-304 -758)">
                  <g transform="translate(438.937 842.549)">
                    <path
                      className="c"
                      d="M-123.937-79.043a.434.434,0,0,0-.159-.343l-3.94-3.94a.486.486,0,0,0-.355-.171.445.445,0,0,0-.465.44.452.452,0,0,0,.135.33l1.554,1.578,1.872,1.713-1.407-.073h-7.782a.434.434,0,0,0-.453.465.44.44,0,0,0,.453.465h7.782l1.407-.073-1.872,1.713-1.554,1.566a.473.473,0,0,0-.135.33.445.445,0,0,0,.465.44.456.456,0,0,0,.33-.147l3.964-3.964A.4.4,0,0,0-123.937-79.043Z"
                    />
                  </g>
                </g>
              </Icon>
              West Port
            </p>
          </div>
          <div className="col-md-4 col-7 ">
            <div className="service">
              Transportation
              <span className="has-tooltip">
                <Icon
                  className="icon "
                  xmlns="http://www.w3.org/2000/svg"
                  width={15}
                  height={15}
                  viewBox="0 0 18 18"
                >
                  <defs>
                    <clipPath id="prefix__a">
                      <path
                        transform="translate(1039 531)"
                        stroke="#707070"
                        fill="#707070"
                        d="M0 0h18v18H0z"
                      />
                    </clipPath>
                    <style>{".prefix__c{fill:#707070}"}</style>
                  </defs>
                  <g
                    transform="translate(-1039 -531)"
                    clipPath="url(#prefix__a)"
                  >
                    <path
                      className="prefix__c"
                      d="M1054.572 533.061h-13.162a2.425 2.425 0 00-2.41 2.425v9.027a2.429 2.429 0 002.428 2.428h13.144a2.429 2.429 0 002.428-2.428v-9.027a2.429 2.429 0 00-2.428-2.425zm.755 10.933a1.309 1.309 0 01-1.327 1.292h-12a1.309 1.309 0 01-1.331-1.295v-7.983a1.309 1.309 0 011.331-1.295h11.979a1.309 1.309 0 011.331 1.295v7.984z"
                    />
                    <path
                      className="prefix__c"
                      d="M1042.794 537.338a.483.483 0 00.486.486h2.9a.486.486 0 000-.971h-2.9a.483.483 0 00-.486.485zM1042.794 540a.483.483 0 00.486.486h9.44a.486.486 0 000-.971h-9.44a.483.483 0 00-.486.485zM1052.7 542.176h-9.42a.486.486 0 100 .971h9.44a.483.483 0 00.486-.486.511.511 0 00-.506-.485z"
                    />
                  </g>
                </Icon>
                <p className="container">
                  <span className="tooltip tooltip-left ">notes</span>
                </p>
              </span>
            </div>
            <div className="service">
              Transportation
              <span className="has-tooltip">
                <Icon
                  className="icon "
                  xmlns="http://www.w3.org/2000/svg"
                  width={15}
                  height={15}
                  viewBox="0 0 18 18"
                >
                  <defs>
                    <clipPath id="prefix__a">
                      <path
                        transform="translate(1039 531)"
                        stroke="#707070"
                        fill="#707070"
                        d="M0 0h18v18H0z"
                      />
                    </clipPath>
                    <style>{".prefix__c{fill:#707070}"}</style>
                  </defs>
                  <g
                    transform="translate(-1039 -531)"
                    clipPath="url(#prefix__a)"
                  >
                    <path
                      className="prefix__c"
                      d="M1054.572 533.061h-13.162a2.425 2.425 0 00-2.41 2.425v9.027a2.429 2.429 0 002.428 2.428h13.144a2.429 2.429 0 002.428-2.428v-9.027a2.429 2.429 0 00-2.428-2.425zm.755 10.933a1.309 1.309 0 01-1.327 1.292h-12a1.309 1.309 0 01-1.331-1.295v-7.983a1.309 1.309 0 011.331-1.295h11.979a1.309 1.309 0 011.331 1.295v7.984z"
                    />
                    <path
                      className="prefix__c"
                      d="M1042.794 537.338a.483.483 0 00.486.486h2.9a.486.486 0 000-.971h-2.9a.483.483 0 00-.486.485zM1042.794 540a.483.483 0 00.486.486h9.44a.486.486 0 000-.971h-9.44a.483.483 0 00-.486.485zM1052.7 542.176h-9.42a.486.486 0 100 .971h9.44a.483.483 0 00.486-.486.511.511 0 00-.506-.485z"
                    />
                  </g>
                </Icon>
                <p className="container">
                  <span className="tooltip tooltip-left ">notes</span>
                </p>
              </span>
            </div>
          </div>
          <div className="col-md-3 col-5 ">
            <div className="price">$150.00</div>
            <div className="price">$150.00</div>
          </div>
        </div>

        <div className="row border">
          <div className="col-md-5 car">
            <div className="row">
              <h5>2020 MercedesBenz C300</h5>
              <Links to="/report">
                <Icon
                  xmlns="http://www.w3.org/2000/svg"
                  xmlnsXlink="http://www.w3.org/1999/xlink"
                  width="14"
                  height="14"
                  viewBox="0 0 14 14"
                  fill="#0078d4"
                >
                  <defs>
                    <clipPath id="a">
                      <rect
                        className="a"
                        width="14"
                        height="14"
                        transform="translate(206.525 319)"
                      />
                    </clipPath>
                  </defs>
                  <g className="b" transform="translate(-206.525 -319)">
                    <g transform="translate(206.525 319)">
                      <path
                        className="c"
                        d="M13.877,4.071,10.086.134a.438.438,0,0,0-.752.3v1.9H9.188A5.694,5.694,0,0,0,3.5,8.021V8.9a.432.432,0,0,0,.341.418.391.391,0,0,0,.1.012.453.453,0,0,0,.4-.249,4.785,4.785,0,0,1,4.3-2.66h.693v1.9a.438.438,0,0,0,.752.3l3.792-3.937A.438.438,0,0,0,13.877,4.071Zm0,0"
                      />
                      <path
                        className="c"
                        d="M12.25,14H1.75A1.752,1.752,0,0,1,0,12.25V4.083a1.752,1.752,0,0,1,1.75-1.75H3.5A.583.583,0,0,1,3.5,3.5H1.75a.584.584,0,0,0-.583.583V12.25a.584.584,0,0,0,.583.583h10.5a.584.584,0,0,0,.583-.583V7.583a.583.583,0,1,1,1.167,0V12.25A1.752,1.752,0,0,1,12.25,14Zm0,0"
                      />
                    </g>
                  </g>
                </Icon>
              </Links>
            </div>
            <p className="fromTo">
              AutoDeal, Inc
              <Icon
                xmlns="http://www.w3.org/2000/svg"
                className="arrow"
                xmlnsXlink="http://www.w3.org/1999/xlink"
                width="11"
                height="11"
                viewBox="0 0 11 11"
                fill="#888"
              >
                <defs>
                  <clipPath id="a">
                    <rect
                      className="a"
                      width="11"
                      height="11"
                      transform="translate(304 758)"
                    />
                  </clipPath>
                </defs>
                <g className="b" transform="translate(-304 -758)">
                  <g transform="translate(438.937 842.549)">
                    <path
                      className="c"
                      d="M-123.937-79.043a.434.434,0,0,0-.159-.343l-3.94-3.94a.486.486,0,0,0-.355-.171.445.445,0,0,0-.465.44.452.452,0,0,0,.135.33l1.554,1.578,1.872,1.713-1.407-.073h-7.782a.434.434,0,0,0-.453.465.44.44,0,0,0,.453.465h7.782l1.407-.073-1.872,1.713-1.554,1.566a.473.473,0,0,0-.135.33.445.445,0,0,0,.465.44.456.456,0,0,0,.33-.147l3.964-3.964A.4.4,0,0,0-123.937-79.043Z"
                    />
                  </g>
                </g>
              </Icon>
              West Port
            </p>
          </div>
          <div className="col-md-4 col-7 ">
            <div className="service">
              Transportation
              <span className="has-tooltip">
                <Icon
                  className="icon "
                  xmlns="http://www.w3.org/2000/svg"
                  width={15}
                  height={15}
                  viewBox="0 0 18 18"
                >
                  <defs>
                    <clipPath id="prefix__a">
                      <path
                        transform="translate(1039 531)"
                        stroke="#707070"
                        fill="#707070"
                        d="M0 0h18v18H0z"
                      />
                    </clipPath>
                    <style>{".prefix__c{fill:#707070}"}</style>
                  </defs>
                  <g
                    transform="translate(-1039 -531)"
                    clipPath="url(#prefix__a)"
                  >
                    <path
                      className="prefix__c"
                      d="M1054.572 533.061h-13.162a2.425 2.425 0 00-2.41 2.425v9.027a2.429 2.429 0 002.428 2.428h13.144a2.429 2.429 0 002.428-2.428v-9.027a2.429 2.429 0 00-2.428-2.425zm.755 10.933a1.309 1.309 0 01-1.327 1.292h-12a1.309 1.309 0 01-1.331-1.295v-7.983a1.309 1.309 0 011.331-1.295h11.979a1.309 1.309 0 011.331 1.295v7.984z"
                    />
                    <path
                      className="prefix__c"
                      d="M1042.794 537.338a.483.483 0 00.486.486h2.9a.486.486 0 000-.971h-2.9a.483.483 0 00-.486.485zM1042.794 540a.483.483 0 00.486.486h9.44a.486.486 0 000-.971h-9.44a.483.483 0 00-.486.485zM1052.7 542.176h-9.42a.486.486 0 100 .971h9.44a.483.483 0 00.486-.486.511.511 0 00-.506-.485z"
                    />
                  </g>
                </Icon>
                <p className="container">
                  <span className="tooltip tooltip-left ">notes</span>
                </p>
              </span>
            </div>
            <div className="service">
              Transportation
              <span className="has-tooltip">
                <Icon
                  className="icon "
                  xmlns="http://www.w3.org/2000/svg"
                  width={15}
                  height={15}
                  viewBox="0 0 18 18"
                >
                  <defs>
                    <clipPath id="prefix__a">
                      <path
                        transform="translate(1039 531)"
                        stroke="#707070"
                        fill="#707070"
                        d="M0 0h18v18H0z"
                      />
                    </clipPath>
                    <style>{".prefix__c{fill:#707070}"}</style>
                  </defs>
                  <g
                    transform="translate(-1039 -531)"
                    clipPath="url(#prefix__a)"
                  >
                    <path
                      className="prefix__c"
                      d="M1054.572 533.061h-13.162a2.425 2.425 0 00-2.41 2.425v9.027a2.429 2.429 0 002.428 2.428h13.144a2.429 2.429 0 002.428-2.428v-9.027a2.429 2.429 0 00-2.428-2.425zm.755 10.933a1.309 1.309 0 01-1.327 1.292h-12a1.309 1.309 0 01-1.331-1.295v-7.983a1.309 1.309 0 011.331-1.295h11.979a1.309 1.309 0 011.331 1.295v7.984z"
                    />
                    <path
                      className="prefix__c"
                      d="M1042.794 537.338a.483.483 0 00.486.486h2.9a.486.486 0 000-.971h-2.9a.483.483 0 00-.486.485zM1042.794 540a.483.483 0 00.486.486h9.44a.486.486 0 000-.971h-9.44a.483.483 0 00-.486.485zM1052.7 542.176h-9.42a.486.486 0 100 .971h9.44a.483.483 0 00.486-.486.511.511 0 00-.506-.485z"
                    />
                  </g>
                </Icon>
                <p className="container">
                  <span className="tooltip tooltip-left ">notes</span>
                </p>
              </span>
            </div>
          </div>
          <div className="col-md-3 col-5 ">
            <div className="price">$150.00</div>
            <div className="price">$150.00</div>
          </div>
        </div>

        <div className="row border">
          <div className="col-md-5 car">
            <div className="row">
              <h5>2020 MercedesBenz C300</h5>
              <Links to="/report">
                <Icon
                  xmlns="http://www.w3.org/2000/svg"
                  xmlnsXlink="http://www.w3.org/1999/xlink"
                  width="14"
                  height="14"
                  viewBox="0 0 14 14"
                  fill="#0078d4"
                >
                  <defs>
                    <clipPath id="a">
                      <rect
                        className="a"
                        width="14"
                        height="14"
                        transform="translate(206.525 319)"
                      />
                    </clipPath>
                  </defs>
                  <g className="b" transform="translate(-206.525 -319)">
                    <g transform="translate(206.525 319)">
                      <path
                        className="c"
                        d="M13.877,4.071,10.086.134a.438.438,0,0,0-.752.3v1.9H9.188A5.694,5.694,0,0,0,3.5,8.021V8.9a.432.432,0,0,0,.341.418.391.391,0,0,0,.1.012.453.453,0,0,0,.4-.249,4.785,4.785,0,0,1,4.3-2.66h.693v1.9a.438.438,0,0,0,.752.3l3.792-3.937A.438.438,0,0,0,13.877,4.071Zm0,0"
                      />
                      <path
                        className="c"
                        d="M12.25,14H1.75A1.752,1.752,0,0,1,0,12.25V4.083a1.752,1.752,0,0,1,1.75-1.75H3.5A.583.583,0,0,1,3.5,3.5H1.75a.584.584,0,0,0-.583.583V12.25a.584.584,0,0,0,.583.583h10.5a.584.584,0,0,0,.583-.583V7.583a.583.583,0,1,1,1.167,0V12.25A1.752,1.752,0,0,1,12.25,14Zm0,0"
                      />
                    </g>
                  </g>
                </Icon>
              </Links>
            </div>
            <p className="fromTo">
              AutoDeal, Inc
              <Icon
                xmlns="http://www.w3.org/2000/svg"
                className="arrow"
                xmlnsXlink="http://www.w3.org/1999/xlink"
                width="11"
                height="11"
                viewBox="0 0 11 11"
                fill="#888"
              >
                <defs>
                  <clipPath id="a">
                    <rect
                      className="a"
                      width="11"
                      height="11"
                      transform="translate(304 758)"
                    />
                  </clipPath>
                </defs>
                <g className="b" transform="translate(-304 -758)">
                  <g transform="translate(438.937 842.549)">
                    <path
                      className="c"
                      d="M-123.937-79.043a.434.434,0,0,0-.159-.343l-3.94-3.94a.486.486,0,0,0-.355-.171.445.445,0,0,0-.465.44.452.452,0,0,0,.135.33l1.554,1.578,1.872,1.713-1.407-.073h-7.782a.434.434,0,0,0-.453.465.44.44,0,0,0,.453.465h7.782l1.407-.073-1.872,1.713-1.554,1.566a.473.473,0,0,0-.135.33.445.445,0,0,0,.465.44.456.456,0,0,0,.33-.147l3.964-3.964A.4.4,0,0,0-123.937-79.043Z"
                    />
                  </g>
                </g>
              </Icon>
              West Port
            </p>
          </div>
          <div className="col-md-4 col-7 ">
            <div className="service">
              Transportation
              <span className="has-tooltip">
                <Icon
                  className="icon "
                  xmlns="http://www.w3.org/2000/svg"
                  width={15}
                  height={15}
                  viewBox="0 0 18 18"
                >
                  <defs>
                    <clipPath id="prefix__a">
                      <path
                        transform="translate(1039 531)"
                        stroke="#707070"
                        fill="#707070"
                        d="M0 0h18v18H0z"
                      />
                    </clipPath>
                    <style>{".prefix__c{fill:#707070}"}</style>
                  </defs>
                  <g
                    transform="translate(-1039 -531)"
                    clipPath="url(#prefix__a)"
                  >
                    <path
                      className="prefix__c"
                      d="M1054.572 533.061h-13.162a2.425 2.425 0 00-2.41 2.425v9.027a2.429 2.429 0 002.428 2.428h13.144a2.429 2.429 0 002.428-2.428v-9.027a2.429 2.429 0 00-2.428-2.425zm.755 10.933a1.309 1.309 0 01-1.327 1.292h-12a1.309 1.309 0 01-1.331-1.295v-7.983a1.309 1.309 0 011.331-1.295h11.979a1.309 1.309 0 011.331 1.295v7.984z"
                    />
                    <path
                      className="prefix__c"
                      d="M1042.794 537.338a.483.483 0 00.486.486h2.9a.486.486 0 000-.971h-2.9a.483.483 0 00-.486.485zM1042.794 540a.483.483 0 00.486.486h9.44a.486.486 0 000-.971h-9.44a.483.483 0 00-.486.485zM1052.7 542.176h-9.42a.486.486 0 100 .971h9.44a.483.483 0 00.486-.486.511.511 0 00-.506-.485z"
                    />
                  </g>
                </Icon>
                <p className="container">
                  <span className="tooltip tooltip-left ">notes</span>
                </p>
              </span>
            </div>
            <div className="service">
              Transportation
              <span className="has-tooltip">
                <Icon
                  className="icon "
                  xmlns="http://www.w3.org/2000/svg"
                  width={15}
                  height={15}
                  viewBox="0 0 18 18"
                >
                  <defs>
                    <clipPath id="prefix__a">
                      <path
                        transform="translate(1039 531)"
                        stroke="#707070"
                        fill="#707070"
                        d="M0 0h18v18H0z"
                      />
                    </clipPath>
                    <style>{".prefix__c{fill:#707070}"}</style>
                  </defs>
                  <g
                    transform="translate(-1039 -531)"
                    clipPath="url(#prefix__a)"
                  >
                    <path
                      className="prefix__c"
                      d="M1054.572 533.061h-13.162a2.425 2.425 0 00-2.41 2.425v9.027a2.429 2.429 0 002.428 2.428h13.144a2.429 2.429 0 002.428-2.428v-9.027a2.429 2.429 0 00-2.428-2.425zm.755 10.933a1.309 1.309 0 01-1.327 1.292h-12a1.309 1.309 0 01-1.331-1.295v-7.983a1.309 1.309 0 011.331-1.295h11.979a1.309 1.309 0 011.331 1.295v7.984z"
                    />
                    <path
                      className="prefix__c"
                      d="M1042.794 537.338a.483.483 0 00.486.486h2.9a.486.486 0 000-.971h-2.9a.483.483 0 00-.486.485zM1042.794 540a.483.483 0 00.486.486h9.44a.486.486 0 000-.971h-9.44a.483.483 0 00-.486.485zM1052.7 542.176h-9.42a.486.486 0 100 .971h9.44a.483.483 0 00.486-.486.511.511 0 00-.506-.485z"
                    />
                  </g>
                </Icon>
                <p className="container">
                  <span className="tooltip tooltip-left ">notes</span>
                </p>
              </span>
            </div>
          </div>
          <div className="col-md-3 col-5 ">
            <div className="price">$150.00</div>
            <div className="price">$150.00</div>
          </div>
        </div>

        <div className="row border">
          <div className="col-md-5 car">
            <div className="row">
              <h5>2020 MercedesBenz C300</h5>
              <Links to="/report">
                <Icon
                  xmlns="http://www.w3.org/2000/svg"
                  xmlnsXlink="http://www.w3.org/1999/xlink"
                  width="14"
                  height="14"
                  viewBox="0 0 14 14"
                  fill="#0078d4"
                >
                  <defs>
                    <clipPath id="a">
                      <rect
                        className="a"
                        width="14"
                        height="14"
                        transform="translate(206.525 319)"
                      />
                    </clipPath>
                  </defs>
                  <g className="b" transform="translate(-206.525 -319)">
                    <g transform="translate(206.525 319)">
                      <path
                        className="c"
                        d="M13.877,4.071,10.086.134a.438.438,0,0,0-.752.3v1.9H9.188A5.694,5.694,0,0,0,3.5,8.021V8.9a.432.432,0,0,0,.341.418.391.391,0,0,0,.1.012.453.453,0,0,0,.4-.249,4.785,4.785,0,0,1,4.3-2.66h.693v1.9a.438.438,0,0,0,.752.3l3.792-3.937A.438.438,0,0,0,13.877,4.071Zm0,0"
                      />
                      <path
                        className="c"
                        d="M12.25,14H1.75A1.752,1.752,0,0,1,0,12.25V4.083a1.752,1.752,0,0,1,1.75-1.75H3.5A.583.583,0,0,1,3.5,3.5H1.75a.584.584,0,0,0-.583.583V12.25a.584.584,0,0,0,.583.583h10.5a.584.584,0,0,0,.583-.583V7.583a.583.583,0,1,1,1.167,0V12.25A1.752,1.752,0,0,1,12.25,14Zm0,0"
                      />
                    </g>
                  </g>
                </Icon>
              </Links>
            </div>
            <p className="fromTo">
              AutoDeal, Inc
              <Icon
                xmlns="http://www.w3.org/2000/svg"
                className="arrow"
                xmlnsXlink="http://www.w3.org/1999/xlink"
                width="11"
                height="11"
                viewBox="0 0 11 11"
                fill="#888"
              >
                <defs>
                  <clipPath id="a">
                    <rect
                      className="a"
                      width="11"
                      height="11"
                      transform="translate(304 758)"
                    />
                  </clipPath>
                </defs>
                <g className="b" transform="translate(-304 -758)">
                  <g transform="translate(438.937 842.549)">
                    <path
                      className="c"
                      d="M-123.937-79.043a.434.434,0,0,0-.159-.343l-3.94-3.94a.486.486,0,0,0-.355-.171.445.445,0,0,0-.465.44.452.452,0,0,0,.135.33l1.554,1.578,1.872,1.713-1.407-.073h-7.782a.434.434,0,0,0-.453.465.44.44,0,0,0,.453.465h7.782l1.407-.073-1.872,1.713-1.554,1.566a.473.473,0,0,0-.135.33.445.445,0,0,0,.465.44.456.456,0,0,0,.33-.147l3.964-3.964A.4.4,0,0,0-123.937-79.043Z"
                    />
                  </g>
                </g>
              </Icon>
              West Port
            </p>
          </div>
          <div className="col-md-4 col-7 ">
            <div className="service">
              Transportation
              <span className="has-tooltip">
                <Icon
                  className="icon "
                  xmlns="http://www.w3.org/2000/svg"
                  width={15}
                  height={15}
                  viewBox="0 0 18 18"
                >
                  <defs>
                    <clipPath id="prefix__a">
                      <path
                        transform="translate(1039 531)"
                        stroke="#707070"
                        fill="#707070"
                        d="M0 0h18v18H0z"
                      />
                    </clipPath>
                    <style>{".prefix__c{fill:#707070}"}</style>
                  </defs>
                  <g
                    transform="translate(-1039 -531)"
                    clipPath="url(#prefix__a)"
                  >
                    <path
                      className="prefix__c"
                      d="M1054.572 533.061h-13.162a2.425 2.425 0 00-2.41 2.425v9.027a2.429 2.429 0 002.428 2.428h13.144a2.429 2.429 0 002.428-2.428v-9.027a2.429 2.429 0 00-2.428-2.425zm.755 10.933a1.309 1.309 0 01-1.327 1.292h-12a1.309 1.309 0 01-1.331-1.295v-7.983a1.309 1.309 0 011.331-1.295h11.979a1.309 1.309 0 011.331 1.295v7.984z"
                    />
                    <path
                      className="prefix__c"
                      d="M1042.794 537.338a.483.483 0 00.486.486h2.9a.486.486 0 000-.971h-2.9a.483.483 0 00-.486.485zM1042.794 540a.483.483 0 00.486.486h9.44a.486.486 0 000-.971h-9.44a.483.483 0 00-.486.485zM1052.7 542.176h-9.42a.486.486 0 100 .971h9.44a.483.483 0 00.486-.486.511.511 0 00-.506-.485z"
                    />
                  </g>
                </Icon>
                <p className="container">
                  <span className="tooltip tooltip-left ">notes</span>
                </p>
              </span>
            </div>
            <div className="service">
              Transportation
              <span className="has-tooltip">
                <Icon
                  className="icon "
                  xmlns="http://www.w3.org/2000/svg"
                  width={15}
                  height={15}
                  viewBox="0 0 18 18"
                >
                  <defs>
                    <clipPath id="prefix__a">
                      <path
                        transform="translate(1039 531)"
                        stroke="#707070"
                        fill="#707070"
                        d="M0 0h18v18H0z"
                      />
                    </clipPath>
                    <style>{".prefix__c{fill:#707070}"}</style>
                  </defs>
                  <g
                    transform="translate(-1039 -531)"
                    clipPath="url(#prefix__a)"
                  >
                    <path
                      className="prefix__c"
                      d="M1054.572 533.061h-13.162a2.425 2.425 0 00-2.41 2.425v9.027a2.429 2.429 0 002.428 2.428h13.144a2.429 2.429 0 002.428-2.428v-9.027a2.429 2.429 0 00-2.428-2.425zm.755 10.933a1.309 1.309 0 01-1.327 1.292h-12a1.309 1.309 0 01-1.331-1.295v-7.983a1.309 1.309 0 011.331-1.295h11.979a1.309 1.309 0 011.331 1.295v7.984z"
                    />
                    <path
                      className="prefix__c"
                      d="M1042.794 537.338a.483.483 0 00.486.486h2.9a.486.486 0 000-.971h-2.9a.483.483 0 00-.486.485zM1042.794 540a.483.483 0 00.486.486h9.44a.486.486 0 000-.971h-9.44a.483.483 0 00-.486.485zM1052.7 542.176h-9.42a.486.486 0 100 .971h9.44a.483.483 0 00.486-.486.511.511 0 00-.506-.485z"
                    />
                  </g>
                </Icon>
                <p className="container">
                  <span className="tooltip tooltip-left ">notes</span>
                </p>
              </span>
            </div>
          </div>
          <div className="col-md-3 col-5 ">
            <div className="price">$150.00</div>
            <div className="price">$150.00</div>
          </div>
        </div>

        <div className="row border">
          <div className="col-md-5 car">
            <div className="row">
              <h5>2020 MercedesBenz C300</h5>
              <Links to="/report">
                <Icon
                  xmlns="http://www.w3.org/2000/svg"
                  xmlnsXlink="http://www.w3.org/1999/xlink"
                  width="14"
                  height="14"
                  viewBox="0 0 14 14"
                  fill="#0078d4"
                >
                  <defs>
                    <clipPath id="a">
                      <rect
                        className="a"
                        width="14"
                        height="14"
                        transform="translate(206.525 319)"
                      />
                    </clipPath>
                  </defs>
                  <g className="b" transform="translate(-206.525 -319)">
                    <g transform="translate(206.525 319)">
                      <path
                        className="c"
                        d="M13.877,4.071,10.086.134a.438.438,0,0,0-.752.3v1.9H9.188A5.694,5.694,0,0,0,3.5,8.021V8.9a.432.432,0,0,0,.341.418.391.391,0,0,0,.1.012.453.453,0,0,0,.4-.249,4.785,4.785,0,0,1,4.3-2.66h.693v1.9a.438.438,0,0,0,.752.3l3.792-3.937A.438.438,0,0,0,13.877,4.071Zm0,0"
                      />
                      <path
                        className="c"
                        d="M12.25,14H1.75A1.752,1.752,0,0,1,0,12.25V4.083a1.752,1.752,0,0,1,1.75-1.75H3.5A.583.583,0,0,1,3.5,3.5H1.75a.584.584,0,0,0-.583.583V12.25a.584.584,0,0,0,.583.583h10.5a.584.584,0,0,0,.583-.583V7.583a.583.583,0,1,1,1.167,0V12.25A1.752,1.752,0,0,1,12.25,14Zm0,0"
                      />
                    </g>
                  </g>
                </Icon>
              </Links>
            </div>
            <p className="fromTo">
              AutoDeal, Inc
              <Icon
                xmlns="http://www.w3.org/2000/svg"
                className="arrow"
                xmlnsXlink="http://www.w3.org/1999/xlink"
                width="11"
                height="11"
                viewBox="0 0 11 11"
                fill="#888"
              >
                <defs>
                  <clipPath id="a">
                    <rect
                      className="a"
                      width="11"
                      height="11"
                      transform="translate(304 758)"
                    />
                  </clipPath>
                </defs>
                <g className="b" transform="translate(-304 -758)">
                  <g transform="translate(438.937 842.549)">
                    <path
                      className="c"
                      d="M-123.937-79.043a.434.434,0,0,0-.159-.343l-3.94-3.94a.486.486,0,0,0-.355-.171.445.445,0,0,0-.465.44.452.452,0,0,0,.135.33l1.554,1.578,1.872,1.713-1.407-.073h-7.782a.434.434,0,0,0-.453.465.44.44,0,0,0,.453.465h7.782l1.407-.073-1.872,1.713-1.554,1.566a.473.473,0,0,0-.135.33.445.445,0,0,0,.465.44.456.456,0,0,0,.33-.147l3.964-3.964A.4.4,0,0,0-123.937-79.043Z"
                    />
                  </g>
                </g>
              </Icon>
              West Port
            </p>
          </div>
          <div className="col-md-4 col-7 ">
            <div className="service">
              Transportation
              <span className="has-tooltip">
                <Icon
                  className="icon "
                  xmlns="http://www.w3.org/2000/svg"
                  width={15}
                  height={15}
                  viewBox="0 0 18 18"
                >
                  <defs>
                    <clipPath id="prefix__a">
                      <path
                        transform="translate(1039 531)"
                        stroke="#707070"
                        fill="#707070"
                        d="M0 0h18v18H0z"
                      />
                    </clipPath>
                    <style>{".prefix__c{fill:#707070}"}</style>
                  </defs>
                  <g
                    transform="translate(-1039 -531)"
                    clipPath="url(#prefix__a)"
                  >
                    <path
                      className="prefix__c"
                      d="M1054.572 533.061h-13.162a2.425 2.425 0 00-2.41 2.425v9.027a2.429 2.429 0 002.428 2.428h13.144a2.429 2.429 0 002.428-2.428v-9.027a2.429 2.429 0 00-2.428-2.425zm.755 10.933a1.309 1.309 0 01-1.327 1.292h-12a1.309 1.309 0 01-1.331-1.295v-7.983a1.309 1.309 0 011.331-1.295h11.979a1.309 1.309 0 011.331 1.295v7.984z"
                    />
                    <path
                      className="prefix__c"
                      d="M1042.794 537.338a.483.483 0 00.486.486h2.9a.486.486 0 000-.971h-2.9a.483.483 0 00-.486.485zM1042.794 540a.483.483 0 00.486.486h9.44a.486.486 0 000-.971h-9.44a.483.483 0 00-.486.485zM1052.7 542.176h-9.42a.486.486 0 100 .971h9.44a.483.483 0 00.486-.486.511.511 0 00-.506-.485z"
                    />
                  </g>
                </Icon>
                <p className="container">
                  <span className="tooltip tooltip-left ">notes</span>
                </p>
              </span>
            </div>
            <div className="service">
              Transportation
              <span className="has-tooltip">
                <Icon
                  className="icon "
                  xmlns="http://www.w3.org/2000/svg"
                  width={15}
                  height={15}
                  viewBox="0 0 18 18"
                >
                  <defs>
                    <clipPath id="prefix__a">
                      <path
                        transform="translate(1039 531)"
                        stroke="#707070"
                        fill="#707070"
                        d="M0 0h18v18H0z"
                      />
                    </clipPath>
                    <style>{".prefix__c{fill:#707070}"}</style>
                  </defs>
                  <g
                    transform="translate(-1039 -531)"
                    clipPath="url(#prefix__a)"
                  >
                    <path
                      className="prefix__c"
                      d="M1054.572 533.061h-13.162a2.425 2.425 0 00-2.41 2.425v9.027a2.429 2.429 0 002.428 2.428h13.144a2.429 2.429 0 002.428-2.428v-9.027a2.429 2.429 0 00-2.428-2.425zm.755 10.933a1.309 1.309 0 01-1.327 1.292h-12a1.309 1.309 0 01-1.331-1.295v-7.983a1.309 1.309 0 011.331-1.295h11.979a1.309 1.309 0 011.331 1.295v7.984z"
                    />
                    <path
                      className="prefix__c"
                      d="M1042.794 537.338a.483.483 0 00.486.486h2.9a.486.486 0 000-.971h-2.9a.483.483 0 00-.486.485zM1042.794 540a.483.483 0 00.486.486h9.44a.486.486 0 000-.971h-9.44a.483.483 0 00-.486.485zM1052.7 542.176h-9.42a.486.486 0 100 .971h9.44a.483.483 0 00.486-.486.511.511 0 00-.506-.485z"
                    />
                  </g>
                </Icon>
                <p className="container">
                  <span className="tooltip tooltip-left ">notes</span>
                </p>
              </span>
            </div>
          </div>
          <div className="col-md-3 col-5 ">
            <div className="price">$150.00</div>
            <div className="price">$150.00</div>
          </div>
        </div>

        <div className="row border">
          <div className="col-md-5 car">
            <div className="row">
              <h5>2020 MercedesBenz C300</h5>
              <Links to="/report">
                <Icon
                  xmlns="http://www.w3.org/2000/svg"
                  xmlnsXlink="http://www.w3.org/1999/xlink"
                  width="14"
                  height="14"
                  viewBox="0 0 14 14"
                  fill="#0078d4"
                >
                  <defs>
                    <clipPath id="a">
                      <rect
                        className="a"
                        width="14"
                        height="14"
                        transform="translate(206.525 319)"
                      />
                    </clipPath>
                  </defs>
                  <g className="b" transform="translate(-206.525 -319)">
                    <g transform="translate(206.525 319)">
                      <path
                        className="c"
                        d="M13.877,4.071,10.086.134a.438.438,0,0,0-.752.3v1.9H9.188A5.694,5.694,0,0,0,3.5,8.021V8.9a.432.432,0,0,0,.341.418.391.391,0,0,0,.1.012.453.453,0,0,0,.4-.249,4.785,4.785,0,0,1,4.3-2.66h.693v1.9a.438.438,0,0,0,.752.3l3.792-3.937A.438.438,0,0,0,13.877,4.071Zm0,0"
                      />
                      <path
                        className="c"
                        d="M12.25,14H1.75A1.752,1.752,0,0,1,0,12.25V4.083a1.752,1.752,0,0,1,1.75-1.75H3.5A.583.583,0,0,1,3.5,3.5H1.75a.584.584,0,0,0-.583.583V12.25a.584.584,0,0,0,.583.583h10.5a.584.584,0,0,0,.583-.583V7.583a.583.583,0,1,1,1.167,0V12.25A1.752,1.752,0,0,1,12.25,14Zm0,0"
                      />
                    </g>
                  </g>
                </Icon>
              </Links>
            </div>
            <p className="fromTo">
              AutoDeal, Inc
              <Icon
                xmlns="http://www.w3.org/2000/svg"
                className="arrow"
                xmlnsXlink="http://www.w3.org/1999/xlink"
                width="11"
                height="11"
                viewBox="0 0 11 11"
                fill="#888"
              >
                <defs>
                  <clipPath id="a">
                    <rect
                      className="a"
                      width="11"
                      height="11"
                      transform="translate(304 758)"
                    />
                  </clipPath>
                </defs>
                <g className="b" transform="translate(-304 -758)">
                  <g transform="translate(438.937 842.549)">
                    <path
                      className="c"
                      d="M-123.937-79.043a.434.434,0,0,0-.159-.343l-3.94-3.94a.486.486,0,0,0-.355-.171.445.445,0,0,0-.465.44.452.452,0,0,0,.135.33l1.554,1.578,1.872,1.713-1.407-.073h-7.782a.434.434,0,0,0-.453.465.44.44,0,0,0,.453.465h7.782l1.407-.073-1.872,1.713-1.554,1.566a.473.473,0,0,0-.135.33.445.445,0,0,0,.465.44.456.456,0,0,0,.33-.147l3.964-3.964A.4.4,0,0,0-123.937-79.043Z"
                    />
                  </g>
                </g>
              </Icon>
              West Port
            </p>
          </div>
          <div className="col-md-4 col-7 ">
            <div className="service">
              Transportation
              <span className="has-tooltip">
                <Icon
                  className="icon "
                  xmlns="http://www.w3.org/2000/svg"
                  width={15}
                  height={15}
                  viewBox="0 0 18 18"
                >
                  <defs>
                    <clipPath id="prefix__a">
                      <path
                        transform="translate(1039 531)"
                        stroke="#707070"
                        fill="#707070"
                        d="M0 0h18v18H0z"
                      />
                    </clipPath>
                    <style>{".prefix__c{fill:#707070}"}</style>
                  </defs>
                  <g
                    transform="translate(-1039 -531)"
                    clipPath="url(#prefix__a)"
                  >
                    <path
                      className="prefix__c"
                      d="M1054.572 533.061h-13.162a2.425 2.425 0 00-2.41 2.425v9.027a2.429 2.429 0 002.428 2.428h13.144a2.429 2.429 0 002.428-2.428v-9.027a2.429 2.429 0 00-2.428-2.425zm.755 10.933a1.309 1.309 0 01-1.327 1.292h-12a1.309 1.309 0 01-1.331-1.295v-7.983a1.309 1.309 0 011.331-1.295h11.979a1.309 1.309 0 011.331 1.295v7.984z"
                    />
                    <path
                      className="prefix__c"
                      d="M1042.794 537.338a.483.483 0 00.486.486h2.9a.486.486 0 000-.971h-2.9a.483.483 0 00-.486.485zM1042.794 540a.483.483 0 00.486.486h9.44a.486.486 0 000-.971h-9.44a.483.483 0 00-.486.485zM1052.7 542.176h-9.42a.486.486 0 100 .971h9.44a.483.483 0 00.486-.486.511.511 0 00-.506-.485z"
                    />
                  </g>
                </Icon>
                <p className="container">
                  <span className="tooltip tooltip-left ">notes</span>
                </p>
              </span>
            </div>
            <div className="service">
              Transportation
              <span className="has-tooltip">
                <Icon
                  className="icon "
                  xmlns="http://www.w3.org/2000/svg"
                  width={15}
                  height={15}
                  viewBox="0 0 18 18"
                >
                  <defs>
                    <clipPath id="prefix__a">
                      <path
                        transform="translate(1039 531)"
                        stroke="#707070"
                        fill="#707070"
                        d="M0 0h18v18H0z"
                      />
                    </clipPath>
                    <style>{".prefix__c{fill:#707070}"}</style>
                  </defs>
                  <g
                    transform="translate(-1039 -531)"
                    clipPath="url(#prefix__a)"
                  >
                    <path
                      className="prefix__c"
                      d="M1054.572 533.061h-13.162a2.425 2.425 0 00-2.41 2.425v9.027a2.429 2.429 0 002.428 2.428h13.144a2.429 2.429 0 002.428-2.428v-9.027a2.429 2.429 0 00-2.428-2.425zm.755 10.933a1.309 1.309 0 01-1.327 1.292h-12a1.309 1.309 0 01-1.331-1.295v-7.983a1.309 1.309 0 011.331-1.295h11.979a1.309 1.309 0 011.331 1.295v7.984z"
                    />
                    <path
                      className="prefix__c"
                      d="M1042.794 537.338a.483.483 0 00.486.486h2.9a.486.486 0 000-.971h-2.9a.483.483 0 00-.486.485zM1042.794 540a.483.483 0 00.486.486h9.44a.486.486 0 000-.971h-9.44a.483.483 0 00-.486.485zM1052.7 542.176h-9.42a.486.486 0 100 .971h9.44a.483.483 0 00.486-.486.511.511 0 00-.506-.485z"
                    />
                  </g>
                </Icon>
                <p className="container">
                  <span className="tooltip tooltip-left ">notes</span>
                </p>
              </span>
            </div>
          </div>
          <div className="col-md-3 col-5 ">
            <div className="price">$150.00</div>
            <div className="price">$150.00</div>
          </div>
        </div>

        {this.state.prices.map((price, id) => (
          <div className="row" key={id}>
            <div className="col-md-5"></div>
            <div className="col-md-4 col-sm-6 col-6" name={price.name}>
              <div className={price.name}>{price.name}</div>
            </div>
            <div className="col-md-3 col-sm-6 col-6" name={price.name}>
              <p className={price.name + "Price"}>
                $
                {price.price.toLocaleString(navigator.language, {
                  minimumFractionDigits: 0,
                })}
              </p>
            </div>
          </div>
        ))}
        <div className="row">
          <div className="col-md-5"></div>
          <div className="col-md-4"></div>
          <div className="col-md-3 col-sm-12 col-12">
            <Button>Pay</Button>
          </div>
        </div>
      </div>
    );
  }
}
