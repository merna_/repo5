import React from "react";
import { InvoiceNavbar } from "./invoiceNavbar/invoiceNavbar";
import { InvoiceFooter } from "./invoiceFooter/invoiceFooter";

export const Layout = (props) => {
  return (
    <>
      <div className={props.className}>
        <br />
        <main className="invoiceContent container">
          <InvoiceNavbar />
          <div className="invoiceBody">{props.children}</div>
          <InvoiceFooter />
        </main>
      </div>
    </>
  );
};
