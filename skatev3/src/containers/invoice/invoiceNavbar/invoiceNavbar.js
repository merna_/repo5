import React from "react";
import { Logo } from "../../../elements/logo/logo";
import "./invoiceNavbar.css";
export const InvoiceNavbar = () => {
  return (
    <div className="invoiceNavbar">
      <Logo className="logo"></Logo>
    </div>
  );
};
