import React from "react";
import "./billingTo.css";
export const BillingTo = () => {
  return (
    <div className="billingTo">
      <h5>Billing To</h5>
      <p>
        Ryets Inc. <br />
        John Doe
        <br /> 332 South Wayside Rd, <br />
        Houston, TX <br />
        United States
      </p>
    </div>
  );
};
