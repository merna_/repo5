import React from "react";
import "./invoiceNumber.css";
export const InvoiceNumber = () => {
  return (
    <div className="invoiceNumber">
      <p>
        Invoice number:<span>128734098</span>
      </p>
      <p>
        Invoice date:<span>02 Jan 2021</span>
      </p>
      <p>
        Due date:<span>07 Jan 2021</span>
      </p>
    </div>
  );
};
