import React from "react";
import { BillingTo } from "./billingTo/billingTo";
import { InvoiceNumber } from "./invoiceNumber/invoiceNumber";

const Billinginfo = () => {
  return (
    <div className="row">
      <div className="col-md-6 col-sm-12 col-12">
        <BillingTo />
      </div>
      <div className="col-md-6 col-sm-10 col-12">
        <InvoiceNumber />
      </div>
    </div>
  );
};

export default Billinginfo;
