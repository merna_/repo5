import React from "react";
import "./invoiceFooter.css";

export const InvoiceFooter = () => {
  return (
    <div className="invoiceFooter ">
      <p className="container">Copyright © 2021 Ryets. All rights reserved.</p>
    </div>
  );
};
