import React from "react";
import { BillingFrom } from "./billingFrom/billingFrom";
import { Layout } from "./layout";
import "./invoice.css";
import Billinginfo from "./billingInfo/billinginfo";
import { InvoiceTable } from "./invoiceTable/invoiceTable";
export const Invoice = () => {
  return (
    <Layout className="invoice">
      <Billinginfo />
      <InvoiceTable/>
      <BillingFrom />
    </Layout>
  );
};
