import React from "react";

export const Logo = (props) => {
  return <div className={props.className}>{props.children}</div>;
};
