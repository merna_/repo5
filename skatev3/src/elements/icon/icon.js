import React from "react";

export const Icon = (props) => {
  return (
    <svg
      xmlns={props.xmlns}
      className={props.className}
      xmlnsXlink={props.xmlnsXlink}
      width={props.width}
      height={props.height}
      viewBox={props.viewBox}
      fill={props.fill}
    >
      {props.children}
    </svg>
  );
};
